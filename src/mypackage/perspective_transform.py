#!/usr/bin/env python

import argparse
import json
import os

import cv2
import numpy as np


class PerspectiveTransform:
    def __init__(self, col_row_count, square_size, target_window_size, ppi, trans_file):
        (self.N_COL, self.N_ROW) = col_row_count
        (self.FOV_W, self.FOV_H) = target_window_size
        self.square_size = square_size
        self.ppi = ppi
        self.M = None
        self.trans_file = trans_file

    def load_M_file(self):
        # for Python 3.8 and higher. 'if ret := ....' can be used to consolidated following two lines
        ret = (os.path.isfile(self.trans_file) and os.access(self.trans_file, os.R_OK))
        if ret:
            self.M = np.loadtxt(self.trans_file, delimiter=',', dtype="float64")
        else:
            print('Transformation matrix data file ({}) missing'.format(self.trans_file))
        return ret

    def calib_to_get_M(self, img_c2m):
        gray = cv2.cvtColor(img_c2m, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(gray, (self.N_ROW, self.N_COL), None)
        corner_indexes = [self.N_COL, -1, -1 - self.N_COL, 0]
        cam_calib_points = np.zeros((4, 2), dtype="float32")
        for i in range(0, 4):
            cam_calib_points[i] = corners[corner_indexes[i]][0]
            print("Corner #{}: {}".format(i, cam_calib_points[i]))
        FOV_XC = int(self.FOV_W / 2)
        FOV_YC = int(self.FOV_H / 2)
        X_CCB_L = FOV_XC - (self.N_COL - 1) / 2 * self.square_size * self.ppi
        X_CCB_R = FOV_XC + (self.N_COL - 1) / 2 * self.square_size * self.ppi
        Y_CCB_T = FOV_YC - (self.N_ROW - 1) / 2 * self.square_size * self.ppi
        Y_CCB_B = FOV_YC + (self.N_ROW - 1) / 2 * self.square_size * self.ppi
        DST_CCB_POINTS = np.array([[X_CCB_R, Y_CCB_B], [X_CCB_L, Y_CCB_B], [X_CCB_L, Y_CCB_T], [X_CCB_R, Y_CCB_T]],
                                  dtype="float32")
        self.M = cv2.getPerspectiveTransform(cam_calib_points, DST_CCB_POINTS)
        print("Transformation Matrix: \n{}".format(self.M))
        self.save_M_file()

    def warp_normalize(self, img_wn):
        return cv2.warpPerspective(img_wn, self.M, (self.FOV_W, self.FOV_H))

    def save_M_file(self):
        np.savetxt(self.trans_file, self.M, delimiter=",")
        save_ok = (os.path.isfile(self.trans_file) and os.access(self.trans_file, os.R_OK))
        if save_ok:
            print('Transformation matrix data file saved as {}'.format(self.trans_file))
        return save_ok


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("work_dir", help="Working folder name", type=str)
    args = parser.parse_args()

    if not args.work_dir.endswith('/'):
        args.work_dir = args.work_dir + '/'

    with open(args.work_dir + '3dscan_cfg.json', "r") as json_file:
        cfg_dict = json.load(json_file)

    PPI = cfg_dict["FOV"]["ppi"]
    FOV_W_IN = cfg_dict["FOV"]["width"]  # inch
    FOV_H_IN = cfg_dict["FOV"]["height"]  # inch
    CTR_H = cfg_dict["checkerboard"]["center_height"]  # inch
    (FOV_W, FOV_H) = (int(FOV_W_IN * PPI), int(FOV_H_IN * PPI))
    (N_ROW, N_COL) = (cfg_dict["checkerboard"]["no_row"], cfg_dict["checkerboard"]["no_col"])

    img = cv2.imread(args.work_dir + cfg_dict["calib_image"])
    if img is not None:
        persp_trans = PerspectiveTransform((N_COL, N_ROW), (FOV_W, FOV_H), PPI, args.work_dir + cfg_dict["tran_matrix_file"])
        persp_trans.calib_to_get_M(img)
        warped = persp_trans.warp_normalize(img)
        cv2.imshow('CameraFeed', img)
        cv2.imshow('Normalized', warped)
        key_input = cv2.waitKey(0) & 0xFF
        cv2.destroyAllWindows()
