import cv2


class LaserMasker:
    def __init__(self, rl_min, h_tgt, h_tol, s_min, bri_lum_min, toshow=False):
        self.rl_min = rl_min
        self.h_tgt = h_tgt
        self.h_tol = h_tol # set to >= 180 to bypass
        self.s_min = s_min
        self.bri_lum_min = bri_lum_min
        self.toshow = toshow
        self.h_min = (self.h_tgt - self.h_tol) % 180
        self.h_max = (self.h_tgt + self.h_tol) % 180

    def mask_bright_red(self, img_mbl):
        blur = cv2.GaussianBlur(img_mbl, (9, 9), 0)
        red_ch = blur[:, :, 2]
        hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
        bright_mask = cv2.inRange(hsv[:, :, 2], self.bri_lum_min, 256)
        if self.h_tol < 180:
            if self.h_max >= self.h_min:
                red_mask = cv2.inRange(hsv, (self.h_min, self.s_min, self.rl_min), (self.h_max, 255, 255))
            else:
                red_mask1 = cv2.inRange(hsv, (0, self.s_min, self.rl_min), (self.h_max, 255, 255))
                red_mask2 = cv2.inRange(hsv, (self.h_min, self.s_min, self.rl_min), (180, 255, 255))
                red_mask = cv2.bitwise_or(red_mask1, red_mask2)
            if self.toshow:
                red_count = (red_mask == 255).sum()
                bright_count = (bright_mask == 255).sum()
                print("Red_count = {}, Bright_count = {}".format(red_count, bright_count))
                cv2.imshow('red_ch', red_ch)
                cv2.imshow('red_mask', red_mask)
                cv2.imshow('bright_mask', bright_mask)
            mask_final = cv2.bitwise_or(red_mask, bright_mask)
            result = cv2.bitwise_and(mask_final, red_ch)
        else:
            result = cv2.bitwise_and(bright_mask, red_ch)
        return result
