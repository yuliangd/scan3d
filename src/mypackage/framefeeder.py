#!/usr/bin/env python

import os
import time
import cv2


class FrameFeeder:
    def __init__(self, feed_type, work_dir, assets, width=1280, height=720, cam_exp=-8):
        self.feed_type = feed_type
        self.work_dir = work_dir
        self.work_assets = assets
        self.image_folder = None
        self.image_files = None
        self.current_file = None
        self.ready = False
        self.frame_index = 0
        self.cap = None
        self.scan_run_id = ''
        feedtype_helper = ['Image Files', 'Video file', 'Webcam']
        error_msg = ''
        # Timelapse Images as Input
        if self.feed_type == 0:
            self.image_folder = self.work_dir + self.work_assets
            if not self.image_folder.endswith('/'):
                self.image_folder = self.image_folder + '/'
            if os.path.exists(self.image_folder):
                self.image_files = os.listdir(self.image_folder)
                self.image_files = [file for file in self.image_files if file.endswith('.jpg') or file.endswith('.png')]
                self.image_files.sort()
                if len(self.image_files) > 0:
                    self.ready = True
                    self.current_file = self.image_files[0]
                    self.scan_run_id = self.current_file[-16:-4]
                else:
                    error_msg = 'No image files found in folder' + self.image_folder + ' Not Found'
            else:
                error_msg = 'Folder ' + self.image_folder + ' Not Found'
        # Video File as Input
        if self.feed_type == 1:
            self.current_file = self.work_dir + self.work_assets
            if (os.path.isfile(self.current_file) and os.access(self.current_file, os.R_OK)):
                self.cap = cv2.VideoCapture(self.current_file)
                self.ready = self.cap.isOpened()
                self.scan_run_id = self.current_file[-16:-4]
            else:
                error_msg = 'File ' + self.current_file + ' Not Found'
        # Video Capture device as input
        if self.feed_type == 2 and self.work_assets.isdigit():
            self.cap = cv2.VideoCapture(int(self.work_assets))  # cv2.CAP_DSHOW doesn't work on Raspberry Pi
            self.cap.set(3, width)
            self.cap.set(4, height) # JetsonNano couldn't handle the camera width and height setting
            self.cap.set(cv2.CAP_PROP_EXPOSURE, cam_exp)
            self.ready = self.cap.isOpened()
            self.scan_run_id = time.strftime('%Y%m%d-%H%M%S')
        if self.ready:
            print("Camera feed ready - {}. File {} loaded".format(feedtype_helper[self.feed_type], self.current_file))
        else:
            error_msg = "Camera feed failed to initiate"
        if error_msg != "":
            print(error_msg)

    def get_next_frame(self):
        frame = None
        if self.feed_type == 1 or self.feed_type == 2:
            ret, frame = self.cap.read()
            self.ready = self.cap.isOpened() and frame is not None
            self.frame_index += 1
        if self.feed_type == 0:
            self.current_file = self.image_files[self.frame_index]
            imgfile = self.image_folder + self.current_file
            frame = cv2.imread(imgfile)
            self.frame_index += 1
            if self.frame_index > 999999:
                self.frame_index = 0
            self.ready = (self.frame_index < len(self.image_files))
        return frame

    def release(self):
        if self.feed_type == 1 or self.feed_type == 2:
            self.cap.release()
