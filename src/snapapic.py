import argparse

import cv2

parser = argparse.ArgumentParser()
parser.add_argument("img_filename", help="Image filename", type=str)
parser.add_argument("--device", "-d", help="Device number", action="store", type=int, default=0)
parser.add_argument("--exposure", "-e", help="Manual Exposure", action="store", type=int, default=-8)
args = parser.parse_args()

img_filename = args.img_filename
if not (img_filename.endswith(".jpg") or img_filename.endswith(".png")):
    img_filename += ".jpg"
cap = cv2.VideoCapture(int(args.device))
cap.set(cv2.CAP_PROP_EXPOSURE, args.exposure)
if cap.isOpened():
    ret, frame = cap.read()
    if ret:
        cv2.imwrite(img_filename, frame)
        print("{} saved".format(img_filename))
    cap.release()
else:
    print("Camera device {} failed to open".format(args.device))
