#!/usr/bin/venv python

"""
This program is to process the 3d scanning footage and calculate the height and volume information.
"""

__author__ = "Yuliang Deng"
__copyright__ = "Copyright 2020, Keirton Inc."

import argparse
import json
import sys
import time
import timeit

import cv2
import numpy as np
from scipy import ndimage

from mypackage.framefeeder import FrameFeeder
from mypackage.imshow_resize import imshow_540
from mypackage.lasermasker import LaserMasker
from mypackage.perspective_transform import PerspectiveTransform


def find_peak_y(img_fpy, line_int, img_mark):
    frame_width = img_fpy.shape[1]
    frame_height = img_fpy.shape[0]
    duringvoid = False
    void_x_list = []
    void_start = None
    y_center = np.full(frame_width, frame_height, dtype=int)
    x_index = np.arange(frame_height)
    for x in range(0, frame_width):
        value = img_fpy[:, x]  # a column
        if value.sum() > 1024:
            y_center[x] = int(np.inner(value, x_index) / value.sum())
            cv2.circle(img_mark, (x, y_center[x]), 1, BGR_RED, -1)
            if duringvoid:
                void_end = x - 1
                void_x_list.append((void_start, void_end))
                duringvoid = False
        else:
            if not duringvoid:
                void_start = x
                duringvoid = True

    if line_int and (len(void_x_list) > 0):
        for (xs, xe) in void_x_list:
            slope = (y_center[xe + 1] - y_center[xs - 1]) / (xe - xs + 2)
            for x in range(xs, xe + 1):
                y_center[x] = y_center[xs - 1] + slope * (x - xs)
    smoothed = ndimage.median_filter(y_center, size=20)
    for x in range(0, frame_width):
        cv2.circle(img_mark, (x, smoothed[x]), 2, BGR_BLUE, -1)
    return smoothed


def baseline_init(framewidth, z):
    x_left = int(framewidth * .25)
    x_right = int(framewidth * .75)
    z_left_avg = int(z[x_left - 50:x_left + 50].mean())
    z_right_avg = int(z[x_right - 50:x_right + 50].mean())
    z_center = (z_left_avg + z_right_avg) / 2
    z_delta = z_right_avg - z_left_avg
    z_ll = int(z_center - z_delta)
    z_rr = int(z_center + z_delta)
    print('Baseline defined: {}px & {}px'.format(z_ll, z_rr))
    return np.linspace(z_ll, z_rr, framewidth, dtype="int")


def calib_peek(img_cp):
    imshow_540('Calibrated Image', img_cp)
    print('Press any key to continue')
    cv2.waitKey(0)
    cv2.destroyWindow('Calibrated Image')


def log_message(lgfile, msg):
    s_msg = time.strftime('%Y-%m-%d %H:%M:%S') + ', ' + msg
    print(s_msg)
    if logfile is not None:
        with open(lgfile, 'a') as the_file:
            the_file.write(s_msg + '\n')


parser = argparse.ArgumentParser()
parser.add_argument("feed_type", help="0: Image Files, 1: Video file, 2: Web-cam", type=int)
parser.add_argument("work_dir", help="Working folder name", type=str)
parser.add_argument("assets", help="Image folder name, or Video file name", type=str)
parser.add_argument("--rate", "-r", help="Sample Rate (Hz)", action="store", type=int, default=999)
parser.add_argument("--fps", help="Display FPS", action="store_true", default=False)
parser.add_argument("--textonly", help="Text line only without display", action="store_true", default=False)
parser.add_argument("--log", "-l", help="Log activities to a file (scan_log.txt)", action="store_true", default=False)
args = parser.parse_args()

if args.feed_type > 2:
    print('Error in Feed-type parameter')
    sys.exit(1)

text_only = args.textonly
live_feed = (args.feed_type == 2)

if args.rate > 0:
    cycle_time = 1 / args.rate
    print("Forced sample rate: {} per sec. Cycle Time: {:.3f} sec".format(args.rate, cycle_time))
else:
    cycle_time = 0

if not args.work_dir.endswith('/'):
    args.work_dir = args.work_dir + '/'

try:
    with open(args.work_dir + '3dscan_cfg.json', "r") as json_file:
        cfg_dict = json.load(json_file)
except IOError as e:
    print('Config file (3dscan_cfg.json) missing. Program aborted.')
    sys.exit(1)

logfile = None
if args.log:
    logfile = args.work_dir + "scan_log.txt"

log_message(logfile, "Program started")

BGR_RED = (0, 0, 255)
BGR_BLUE = (255, 0, 0)
BGR_YELLOW = (0, 255, 255)
TEXT_ON_IMAGE = cfg_dict["text_overlay"]
PPI = cfg_dict["FOV"]["ppi"]
X_STEP = 1 / PPI
FOV_W_IN = cfg_dict["FOV"]["width"]  # inch
FOV_H_IN = cfg_dict["FOV"]["height"]  # inch
max_height = cfg_dict["FOV"]["obj_max_height"]
CTR_H = cfg_dict["checkerboard"]["center_height"]  # inch
(FOV_W, FOV_H) = (int(FOV_W_IN * PPI), int(FOV_H_IN * PPI))
(N_ROW, N_COL) = (cfg_dict["checkerboard"]["no_row"], cfg_dict["checkerboard"]["no_col"])
check_sq_size = cfg_dict["checkerboard"]["square_size"]
FOV_YC = int(FOV_H / 2)
Y_BASE = int(FOV_YC + CTR_H * PPI)
opcua_on = "OPCUA" in cfg_dict
crop_row_start = max(int(Y_BASE - (max_height + 0.5) * PPI), 0)
calib_image_file = args.work_dir + "calib_board.jpg"
z_baseline = np.full(FOV_W, (Y_BASE - crop_row_start), dtype="int")
persp_trans = PerspectiveTransform((N_COL, N_ROW), check_sq_size, (FOV_W, FOV_H), PPI,
                                   args.work_dir + "cam_trans_matrix.csv")
(cam_width, cam_height) = (1280, 720)
cam_exp = 0
if "camera" in cfg_dict:
    cam_width = cfg_dict["camera"]["width"]
    cam_height = cfg_dict["camera"]["width"]
    cam_exp = cfg_dict["camera"]["exposure"]

trans_file_ok = persp_trans.load_M_file()
if trans_file_ok:
    log_message(logfile, "Transformation file (" + persp_trans.trans_file + ") loaded OK")
else:
    log_message(logfile, "Transformation file (" + persp_trans.trans_file + ") missing")
    img = cv2.imread(calib_image_file)
    if img is not None:
        persp_trans.calib_to_get_M(img)
        warped = persp_trans.warp_normalize(img)
        log_message(logfile, "Transformation file reconstructed based on calibration image")
        if not text_only:
            calib_peek(warped)
    else:
        log_message(logfile, "Calibration Image (" + calib_image_file + "missing or invalid")
        sys.exit(404)

ls_mask = LaserMasker(cfg_dict["color"]["red_min"], cfg_dict["color"]["hue_target"],
                      cfg_dict["color"]["hue_tolerance"], cfg_dict["color"]["sat_min"],
                      cfg_dict["color"]["lum_min"])
feeder = FrameFeeder(args.feed_type, args.work_dir, args.assets, cam_width, cam_height, cam_exp)

opcua_ok = False
if opcua_on:
    from opcua import Client

    opcua_client = Client(cfg_dict["OPCUA"].get("url_endpoint"))
    print("Trying to connect to OPC-UA server...")
    try:
        opcua_client.connect()
        log_message(logfile, "OPC UA Connected")
        belt_finished = False
        var_area = opcua_client.get_node(cfg_dict["OPCUA"].get("node_area"))
        var_heartbeat = opcua_client.get_node(cfg_dict["OPCUA"].get("node_heartbeat"))
        opcua_ok = True
    except:
        log_message(logfile, "Failed to connect to OPCUA server. Program aborted")
        sys.exit()

user_break = False

heartbeat = False
while feeder.ready:
    overlay_txt = []
    init_status = (feeder.frame_index == 0)
    start_time = timeit.default_timer()
    if args.fps:
        if not init_status:
            process_fps = 1 / (start_time - last_frame_start)
            print("Processing FPS = {:.1f}".format(process_fps))
        last_frame_start = start_time
    img = feeder.get_next_frame()
    if img is not None:
        if not text_only:
            imshow_540('CameraFeed', img)
        if persp_trans.M is None:
            key_input = cv2.waitKey(1) & 0xFF
            if key_input == ord('c') or key_input == ord('C'):
                persp_trans.calib_to_get_M(img)
                warped = persp_trans.warp_normalize(img)
                cv2.imwrite(calib_image_file, img)
                calib_peek(warped)
                feeder.frame_index = 0
                log_message(logfile, "(Re)Calibrated")
        else:
            warped = persp_trans.warp_normalize(img)
            crop = warped[crop_row_start:FOV_H, :, :]
            img_redline = ls_mask.mask_bright_red(crop)
            redline_ratio = (img_redline > 0).sum() / (img_redline.shape[0] * img_redline.shape[1])
            z_redline = find_peak_y(img_redline, line_int=True, img_mark=crop)
            if init_status and not live_feed:
                log_message(logfile, "Scan Run ID: " + feeder.scan_run_id)
            else:
                cv2.line(crop, (0, z_baseline[0]), (FOV_W, z_baseline[-1]), BGR_YELLOW, thickness=1)
                z_inch_array = (z_baseline - z_redline) / PPI
                z_inch_array[z_inch_array < 0] = 0
                current_area = round(max(z_inch_array.sum(axis=0) / PPI, 0), 2)
                peak_height = round(z_inch_array.max(), 2)
                overlay_txt.append('Peak Height = ' + str(peak_height) + ' in')
                overlay_txt.append('XSection Area = ' + str(current_area) + ' sq-in')
                overlay_txt.append('Redline Ratio = ' + str(round(redline_ratio, 5)))
                if opcua_ok:
                    var_area.set_value(current_area)
                    var_heartbeat.set_value(heartbeat)
                if not text_only:
                    imshow_540('Normalized View', crop, overlay_txt)
                else:
                    print(overlay_txt)
    if not text_only:
        key_input = cv2.waitKey(1) & 0xFF
        if key_input == 27:
            user_break = True
            break
        elif key_input == ord(' '):  # Press space to pause
            cv2.waitKey(0)
        elif key_input == ord('b') or key_input == 'B':  # re-define baseline_init
            z_baseline = baseline_init(FOV_W, z_redline)
        elif (key_input == ord('r') or key_input == ord('R')) and live_feed:  # R for re-calibrate
            persp_trans.M = None
            print('Forced reset for re-calibration')
    elapsed_time = timeit.default_timer() - start_time
    time_to_sleep = max(0, cycle_time - elapsed_time)
    time.sleep(time_to_sleep)
    heartbeat = not heartbeat

feeder.release()

if not text_only:
    cv2.destroyAllWindows()
    log_message(logfile, "Closing all graphics windows")

if opcua_on:
    opcua_client.disconnect()
    log_message(logfile, "OPCUA disconnected peacefully")

log_message(logfile, "Program exited manually")
