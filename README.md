# A 3D Scanning Program Using Laser Triagulation Technique

3dscan_datk.py is the main program. It can take 3 types of input:

- pre-captured timelapse images (*.jpg, *.png),
- pre-captured video file (*.mp4, *.avi),
- live camera feed (Device 0/1/2 etc)

## Configuration and Preparation

- Create a work folder
- Ensure there's a copy of file '3dscan_cfg.json' in it.
- For running with pre-captured feed, put a calib_board.jpg image in the work folder
- Put images in a folder under the work folder
- Video file can be put in the work folder


## How to launch it?

The script takes 3 command line arguments (parameters).

1. Feed type (0: image, 1: video, 2: live-feed)
2. Work folder
3. Assets (image folder, video file, or capture device index)

### Examples
- Timelapse images
`python 3dscan_datk.py 0 /work_folder/ images_folder`

- Video file
`python 3dscan_datk.py 1 /work_folder/ videofile.mp4`

- Live feed
`python 3dscan_datk.py 2 /work_folder/ 0`


## Output
- Cross-section area will be displayed on screen in real-time
- Point-clouds of 3d coordinates files can be generated, if enabled in the '3dscan_cfg.json' file
- Heightmap can be displayed and saved, if enabled in the '3dscan_cfg.json' file

## Environment
- Python 3.5+
- OpenCV 4+
- Numpy
- Scipy